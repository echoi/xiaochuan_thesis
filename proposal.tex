\documentclass[12pt]{article}

\usepackage{graphics}
\usepackage{epsfig}
\usepackage{times}
\usepackage{amsmath}

%%------------------------------------------------
%   For trackchanges package
%   Change the editor name as you like
%%------------------------------------------------
\usepackage[inline]{trackchanges}
\addeditor{XT}
\addeditor{EC}
%%------------------------------------------------
% Try one of these commands:
%     \add[editor]{added text}
%     \remove[editor]{removed text}
%     \change[editor]{removed text}{added text}
%     \note[editor]{note text}
%     \annote[editor]{text to annotate}{note text}
%%------------------------------------------------

% Borrowed the template from the CS dept. of Columbua University.
% <http://psl.cs.columbia.edu/phdczar/proposal.html>:
%
% The standard departmental thesis proposal format is the following:
%        30 pages
%        12 point type
%        1 inch margins all around = 6.5   inch column
%        (Total:  30 * 6.5   = 195 page-inches)
%
% For letter-size paper: 8.5 in x 11 in
% Latex Origin is 1''/1'', so measurements are relative to this.

\topmargin      0.0in
\headheight     0.0in
\headsep        0.0in
\oddsidemargin  0.0in
\evensidemargin 0.0in
\textheight     9.0in
\textwidth      6.5in

\title{{\bf 3D Numerical Models for Along-axis Variations in Faulting at Slow- to Intermediate-spreading Mid-Ocean Ridges} \\
\it Thesis proposal}
\author{ {\bf Xiaochuan Tian}  \\
Center for Earthquake Research and Information \\
The University of Memphis\\
{\small xtian@memphis.edu}
}
\date{\today}

\begin{document}
\pagestyle{plain}
\pagenumbering{roman}
\maketitle

\pagebreak
\begin{abstract}

Bathymetry observations show great variety of morphologies at Mid-ocean Ridges.
Based on recent studies, the variety of faulting patterns, crustal structures and axial
relief at Mid-ocean Ridges are mainly controlled by magma supply and plate extension
rate. Previous studies expressed the relation between them as a factor M that varies
from 0 to 1 (1 means that the plate extension is fully accommodated by magma
accretion while 0 means rifting with no magma supply). They successfully predicted
many observations across axis. However, interactions between tectonics and
magmatism at MORs are inevitably three-dimensional processes (both across and
along ridge axis). Here, in order to better understand the mechanism of the processes,
we propose to model the processes with a 3D numerical modeling code, SNAC.

\end{abstract}

\pagebreak
\tableofcontents
\pagebreak

\cleardoublepage
\pagenumbering{arabic}

\section{Introduction}
\label{ch:intro}

\note[EC]{This long introduction is mixed with your proposed work. Like I asked below, make sure to distinguish motivation, previous work, your own work to do.}

The mid-ocean ridges (MORs) are the longest mountains chains on the Earth with total
length of about 60,000 km. Both along and across MORs axis, varied topography was
observed from multi-beam bathymetric data.

As more and more geophysical surveys (GPS, Bathymetry, Gravity, Resistivity,
Seismology, Drilling) are conducted on MORs globally, we gradually attain detailed
information of MORs structures and the mechanisms that create them. Studies of
ophiolite give us hints of the layered structure of the oceanic crust. GPS monitoring on a
global tectonic scale give us the separation velocity at diverging boundaries. Gravity
surveys provide us density variation along and across axis; Resistivity surveys yield
lower resistivity results under mid-ocean ridges; Seismology reflection shows reflector
while refraction shows lower traveling velocities and stronger attenuations when seismic
waves propagate through regions beneath MORs; Experiments conducted in laboratory
show the seismic velocity slows down when partial melt exists. All these geophysical
observations help us not only better constrain the geometry of the structure but also
understand the physical conditions of the materials beneath MORs (i.e. temperature
profile, brittle-ductile transition depth, material strength), which in turn, provide us
important information for setting initial and boundary conditions of the model .

Based on the geophysical evidences and several models (Seismic-velocity models;
density model; thermal reaction models), we know that various topography are mainly
controlled by 4 factors: 1) magma supply, 2) tectonic strain, 3) hydrothermal circulation
and 4) spreading rate. We also know that the spreading rate of the plate separation is
the dominant factor (Fowler, 2004). However, many constraining details are remain
unknown. For example, the spatial distribution and temporal variation of diking, although
can be probed by geophysical surveys, the timing as well as the mechanism of diking,
which is a key factor of our study, is still not very clear. What is more, the thermal
structure of the oceanic crust, especially near axis, with all the tectonics, magmatism
and hydrothermal circulations processes going on, is hard to be explored accurately,
however is the key to the material properties, especially the strength of the material
which has essential influences on the faulting process. Although many important
informations remain unclear, to the first order, we can simplified the process and focus
on the major significance of the process (i.e. the M-factor).

From a large sum of bathymetric surveys, we know that there are two end members of
ridges with respect to the spreading rate: one is median rift valley (10~20km wide,
1~2km deep) for slow-spreading centers like Mid-Atlantic Ridges (Fig.1); The other is
fast-spreading centers with much smoother crests and usually axial highs (10~20 km
wide, 0.3-0.5 km high) like East Pacific Rise (Fig. 2). Meanwhile, there are many
intermediate cases between those two end members. In addition, arbitrarily picked three
cross sections (Figs.1 d), e), f)) along Mid-Atlantic Ridge varies distinctly with respect to
the width, depth of the median valleys and the off-axis morphology. To study how do
these dramatically diverse morphologies come into being is the goal of this study. We
propose that, to the first order, they are the surface expressions of the spatial and
temporal distributions of the normal faulting on each side of the ridge axis which is
controlled by the ‘M-factor’.

\subsection{Related work}
\label{ch:related}

The Mid-Atlantic Ridge was discovered while the first trans-Atlantic telegraph cable was
being laid. The East Pacific Rise was discovered by H. M. S. Challenger in 1875.
(Fowler, 2004). As more and more deep sea surveys were carried out and data become
available, studies trying to explain the mechanism of the observations followed up.

According to Solomon et al. (1988), low level of seismic moment release at slow
spreading centers suggests that dyke opening accommodates most plate separation at
ridges with axial valleys. However, early plate stretching models did not take dyke
opening into account. The first model that consider magmatic dyke intrusion is from
studies by Chen and Morgan (1990), who assumed that dykes widen at a rate equal to
the plate separation rate. In their model dykes open from the surface down to the base
of the crust. An axial valley was seen only when the axial mantle was cool enough to be
brittle. The depth of the resulting valley scaled with thickness of the axial mantle
lithosphere. This model successfully predicts that the increase in spreading rate will
result in decrease in axial valley relief.

However, limitations in the model of Chen and Morgan (1990) were revealed by new
observations and modeling results during the last decades. First, the model’s assumed
distribution of dyke opening is not consistent with the fluid mechanics of magma
intrusion. Low viscosity magma should inject into regions with low stresses (Rubin and
Pollard, 1987) while the sub-crustal mantle lithosphere develops very low stresses in
the model. Second, due to sufficient magma supply, the model crust is not stretched.
Thus, the crust should be little faulted as moving away from the valley. Third, in the
model, the absence of an axial valley and related tectonic faulting is attributed to the
presence of magma chamber, however, faulting and fault-bounded axial valleys that
overlie axial magma chambers have been identified at intermediate-spreading-rate
segments (e.g., Juan de Fuca and Southeast Indian Ridges) and slow-spreading ridges
(e.g., portions of the Mid-Atlantic Ridge; Singh et al., 2006; Fig 3).

In addition, scientific interests in oceanic core complexes (OCC) has dramatically
increased after the Atlantis Massif (Fig. 4) was first discovered during an expedition in
1966 % follow the standard citing style: (http://en.wikipedia.org/wiki/Oceanic_core_complex). 
There are several important
research topics related to Atlantis Massif for its unique characteristics, especially for that
it brought mantle materials to a depth within human’s ability to drill in the deep ocean.
How does such a great massif formed during the process of interactions among
magmatic supply, tectonic faulting and plate spreading, which is another focus of our
study, is under extensive studies and still remain mysterious.

Partly inspired by the OCCs observations, Buck et al. (2005) developed a simple way to
treat the process. In that model, dyke opening in the lithosphere is taken to be uniform
with depth, avoiding the discontinuity in stresses at the base of the crust inherent in
Chen and Morgan (1990). The rate of dyke opening is expressed as a ratio, M, between
the rate of magmatic supply which accommodates the plate separation and the rate of
plate separation. There are four types of faulting patterns predicted by the models (Buck
et al., 2005; Tucholke et al., 2008). For M=1, magmatic accretion accommodates all the
plate separation, axial lithospheric separation is all taken up by dyke opening and thus
no axial faults and valley developed, instead, axial high are generated (Fig. 5). For
0.5<M<1.0, more than half of the plate separation is accommodated by magmatism.
The integrated strength of a fault increase as lithosphere cools and thickens with
distance from the axis, so the fault eventually becomes inactive and is replaced by a
new, near-axis fault (Fig. 6; Fig. 7A). For M between about 0.3 to 0.5, long lived
detachment faults are presented and most likely to form OCCs (Fig. 7B; Fig. 7C). For
M<=0.2, the seafloor may consist almost entirely of fault surfaces (Fig. 7F) which is
similar to the large tracts of seafloor offset by high-angle normal faults at about 10 km
spacing on the Southwest Indian Ridge (Cannat et al., 2006). Since these M-based
models are well consistent with observations, our study in 3D modeling along the MORs
will be based on their results and model set-ups.

\section{\annote{Proposal Topic}{or Proposed Work}}
\label{ch:proposal}

\note[EC]{The content of your proposal. Separate all of the work-to-do parts from the existing text and put them here.}

\subsection{Method}
\label{ch:method}

The goal of our study is to understand how does spatial and temporal distributions of
interactions between magmatism and tectonics control the morphology seen at MORs in
a three dimensional perspective. We apply a 3D numerical modeling code—SNAC
(written and maintained by Dr. Choi) to model the process. Meanwhile, we utilize
geophysical observations and published results to constrain the set-ups of the model.
By iteratively minimizing the gap between results from experiments and observations,
hopefully, a model with consistent results will be generated. Then, we will interpret the
observations based on that model.

SNAC is an explicit Lagrangian finite element code. Meanwhile, it is a reusable set of
libraries or classes for a software system. It uses the energy-based finite element
method to solve the force balance equation for elasto-visco-plastic materials (Bathe,
1996).

Because of the expense of running 3D models, magmatic dyke opening will be treated
in a simpler ‘continuous opening’ way which is the same as M-factor in (Buck et al.,
2005). A number of trial models will be run to find suitable model settings such as
thermal and kinematic initial and boundary conditions, viscosity structure and material
properties. At this stage, the computational cost will be kept as low as possible by
running them for a short time or in a low resolution. Then, we will systematically
examine different scenarios of variable M along the ridge.

\section{Research plan}
\label{ch:plan}

\note[EC]{Provide an overview of what you have done and what need to be done.}

Table \ref{tab:plan} shows my plan for completion of the research.

\begin{table}[hc]
\begin{small}
\begin{center}
\begin{tabular}{lll}
Timeline & Work & Progress\\
\hline
          & XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX & completed\\
Nov. xxxx & XXXXXXXXXXXXXXXXXXXXXXXXXXX & ongoing\\
Jan. xxxx & Thesis writting & \\
Feb. xxxx & Thesis defense & \\
\end{tabular}
\end{center}
\end{small}
\caption{Plan for completion of my research}
\label{tab:plan}
\end{table}

Thus, I plan to defend my thesis in XXX XXXX.

\pagebreak

\begin{footnotesize}
\note[EC]{If you know how to use bibtex, modify this part appropriately.}
\bibliographystyle{plain}
\bibliography{string,itu,rfc,i-d}

References \\
Bathe, K.-J., 1996, Finite Element Procedure. Prentice-Hall, Upper Saddle River, NJ. \\
Buck, W. R., Lavier, L. L., and Poliakov A. N. B., 2005, Modes of faulting at mid-ocean
ridges, Nature, v. 434, p. 719-723. \\
Cannat, M., Sauter, D., Mendel, V., Ruellan, E., Okino, K., Escartin, J., Combier, V., and
Baala, M., 2006, Modes of seafloor generation at a melt-poor ultraslow-spreading ridge,
Geology, v. 34(7). \\
Chen, Y., and Morgan, W. J., 1990, Rift valley/no rift valley transition at mid-ocean
ridges, J. Geophys. Res., 95, 17, 571-17, 581. \\
Choi, E.-s., Lavier, L., and Gurnis, M., 2008, Thermomechanics of mid-ocean ridge
segmentation, Physics of the Earth and Planetary Interiors, dpi:10.1016/j.pepi.
2008.08.010 \\
Fowler, C. M. R., 2004, The Solid Earth: An Introduction to Global Geophysics,
Cambridge University Press, p. 391-439. \\
Rubin, A. M., and Pollard, D. D., 1987, Origins of Blake-Like Dykes in Volcanic Rift
Zones, U. S. G. S. Professional Paper ,1350. \\
Singh, S. C., Crawford, W. C., Carton, H., Sehr, T., Combier, V., Cannat, M., Canales, J.
P., 2006, Discovery of a magma chamber and faults beneath a Mid-Atlantic Ridge
hydrothermal field, Nature, v. 442(7106), p. 1029-1032. \\
Solomon, S. C., Huang, P. Y., and Meinke, L., 1988, The seismic moment budget of
slowly spreading ridges, Nature, v. 334(6177), p. 58-60. \\
Tucholke, B. E., Behn, M. D., Buck, W. R., and Lin, J., 2008, Role of melt supply in
oceanic detachment faulting and formation of megamullions, Geology, v. 36, p. 455-458.

\end{footnotesize}

\end{document}


